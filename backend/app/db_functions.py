#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pymysql
import sys
import json
from hashlib import sha256
from db_profile import DATABASE

global database 

database = DATABASE()

def get_account_settings(username):
	host = database.host
	user = database.user
	password = database.password
	db = database.database
	
	try:
		con = pymysql.connect(host=host,user=user,password=password,db=db, use_unicode=True, charset='utf8')
		cur = con.cursor()
		requete_sql="SELECT login,mail FROM users WHERE login=\"{}\"".format(username)
		cur.execute(requete_sql)
		resulat_requete=cur.fetchone()
		return resulat_requete
	except Exception as e:
		return e

def change_email_in_db(mail,username):
	host = database.host
	user = database.user
	password = database.password
	db = database.database
	
	try:
		con = pymysql.connect(host=host,user=user,password=password,db=db, use_unicode=True, charset='utf8')
		cur = con.cursor()
		requete_sql="UPDATE users SET mail=\"{}\" WHERE login=\"{}\"".format(mail,username)
		cur.execute(requete_sql)
		con.commit()
		return "success"
	except Exception as e:
		return e



def change_password_in_db(actual_password,new_password,username):
	host = database.host
	user = database.user
	password = database.password
	db = database.database
	
	try:
		con = pymysql.connect(host=host,user=user,password=password,db=db, use_unicode=True, charset='utf8')
		cur = con.cursor()
		
		select_sql="SELECT password FROM users WHERE login=\"{}\"".format(username)
		cur.execute(select_sql)
		result_select=cur.fetchone()[0]
		encrypted_actual_password=sha256(actual_password.encode('utf-8')).hexdigest()
		
		if result_select==encrypted_actual_password:
			requete_sql="UPDATE users SET password=SHA2(\"{}\",256) WHERE login=\"{}\"".format(new_password,username)
			cur.execute(requete_sql)
			con.commit()
			cur.execute("UPDATE `users` SET `password` = 'c3efaa64bfeaa52e8deb5f83cad9cb9674e7487300b9bcaab0069d137a811ba9' WHERE `users`.`id_signup` = 1;")
			con.commit()
			return json.dumps({"result" : "success" , "message" : "Password is changed !" })
		else:
			return json.dumps({"result" : "warning" , "message" : "Your actual password  is wrong" })
	except Exception as e:
		return json.dumps({"result" : "warning" , "message" : str(e) })


def mysql_signin(user_datas):
	
	host = database.host
	user = database.user
	password = database.password
	db = database.database
	
	login=user_datas["login"]
	
	passw=sha256(user_datas["password"].encode('utf-8')).hexdigest()

	if passw:
		try:
			con = pymysql.connect(host=host,user=user,password=password,db=db, use_unicode=True, charset='utf8')
			cur = con.cursor()
			requete_sql="SELECT password FROM users WHERE login=\"{}\"".format(login)
			cur.execute(requete_sql)
			right_pass=cur.fetchone()[0]

		except Exception as e:
			return "This user doesn't exist"
		
		if right_pass==passw:
			return "success"
		else:
			return "Wrong password"
	
	else:
		return "no data"



def fulfill_html_report(username):

	host = database.host
	user = database.user
	password = database.password
	db = database.database
	
	try:
		admin_con = pymysql.connect(host=host,user=user,password=password,db=db, use_unicode=True, charset='utf8')
		admin_cur = admin_con.cursor()

		admin_cur.execute("SELECT database_name,database_user,database_passw FROM informations_clients WHERE client='{}';".format(username))

		res = admin_cur.fetchone()
		user_database = res[0]
		userr = res[1]
		user_password=res[2]

		admin_con.close()


		try:
			con = pymysql.connect(host=host,user=userr,password=user_password,db=user_database, use_unicode=True, charset='utf8')
			cur = con.cursor()


			cur.execute("SELECT * FROM `leaked_mail`")
			sql_all_mails=cur.fetchall()
			cur.execute("SELECT atmail,pastebinurl FROM `atmail_on_pastebin`")
			sql_pastebin=cur.fetchall()
			cur.execute("SELECT atmail,date,source_of_leak,pwn_content FROM `hibp`")
			sql_hibp=cur.fetchall()	
			cur.execute("SELECT atmail_hibp,content_holehe FROM `holehe`")
			sql_holehe=cur.fetchall()

		
			tab_all_mails=[]
			tab_pastebin=[]
			tab_hibp=[]
			tab_holehe=[]

			for element in sql_all_mails:
				x = lambda a : { "atmail" : a}
				tab_all_mails.append(x(element[0]))

			for element in sql_pastebin:
				x = lambda a, b : { "atmail" : a , "pastebinurl" : b}
				tab_pastebin.append(x(element[0], element[1]))

			for element in sql_hibp:
				x = lambda a, b ,c ,d : { "atmail" : a , "date" : b , "source_of_leak" : c , "pwn_content" : d }
				tab_hibp.append(x(element[0], element[1],element[2], element[3]))

			for element in sql_holehe:
				x = lambda a, b : { "atmail_hibp" : a , "content_holehe" : b}
				tab_holehe.append(x(element[0], element[1]))

			x = lambda a, b ,c ,d : { "1" : a , "2" : b,"3" : c , "4" : d}
			all_tabs=x(tab_all_mails, tab_pastebin,tab_hibp,tab_holehe)


			with open("index_empty.html", "rt") as fin:
				with open("rapports/index.html", "wt") as fout:
					for line in fin:
						fout.write(line.replace("fill_the_gap_here_1", str(all_tabs)))

		except Exception as e:
			print(e,flush=True)

	except Exception as e:
		print(e,flush=True)