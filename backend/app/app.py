#!/usr/bin/env python
# -*- coding: utf-8 -*-

from get_datas import *
from signup_functions import *
from db_functions import *
from functools import wraps
from flask import Flask, json, request,redirect,jsonify,session,make_response,send_file
import os
from pprint import pprint
import jwt
import datetime


app = Flask(__name__)
app.config["SECRET_KEY"]="demo"


def check_for_token(func):
	@wraps(func)
	def wrapped(*args,**kwargs):
		token = request.headers["authorization"]
		if not token:
			return jsonify({'message':'missing token'}),403
		try:
			data = jwt.decode(token,key=app.config["SECRET_KEY"],algorithms=["HS256"])
		except ValueError:
			print(ValueError)
			return jsonify({'message':'invalid token'}),403
		return func(*args,**kwargs)
	return wrapped
	

	
@app.route("/api/signup",methods=["POST"])
def signup():
	print(request.json)
	result_insert=signup_user_bdd(request.json)
	response = jsonify(message=result_insert)
	response.headers.add("Access-Control-Allow-Origin","*")
	response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
	print("_____________________________")	
	return response


@app.route("/api/download")
@check_for_token
def download():
	token = request.headers["authorization"]
	data = jwt.decode(token,key=app.config["SECRET_KEY"],algorithms=["HS256"])
	if os.path.exists("./rapports/index.html"):
		os.remove("./rapports/index.html")
	fulfill_html_report(data["user"])
	file="rapports/index.html"
	return send_file(file,as_attachment=True)




@app.route("/api/signin",methods=["POST"])
def signin():
	print("HELLOOOOOOOOOOO1",flush=True)

	result_insert=mysql_signin(request.json)
	print("HELLOOOOOOOOOOO2",flush=True)
	if result_insert=="success":
		session["logged_in"]=True
		token=jwt.encode({
			'user' : request.json["login"],
			'exp' : datetime.datetime.utcnow() + datetime.timedelta(seconds=50000)
		},
		app.config["SECRET_KEY"])
		response = jsonify({'token': token})
		print("_____________________________")
		return response
	else:
		response = jsonify({'error': result_insert})
		return response

	return response



@app.route("/api/getdatas/1",methods=["GET"])
@check_for_token
def getdatas1():
	if request.method == "GET":
		token = request.headers["authorization"]
		data = jwt.decode(token,key=app.config["SECRET_KEY"],algorithms=["HS256"])
		result=get_datas_from_db_1(data["user"])
		fulfill_html_report(data["user"])
		print("result1 :"+str(result),flush=True)
		response=jsonify(result)
		return response
	return "ERRROR HERE"
	
@app.route("/api/getdatas/2",methods=["GET"])
@check_for_token
def getdatas2():
	if request.method == "GET":
		token = request.headers["authorization"]
		data = jwt.decode(token,key=app.config["SECRET_KEY"],algorithms=["HS256"])
		result=get_datas_from_db_2(data["user"])
		response=jsonify(result)
		return response
		
	return "ERRROR HERE"
	
@app.route("/api/getdatas/3",methods=["GET"])
@check_for_token
def getdatas3():

	if request.method == "GET":
		token = request.headers["authorization"]
		data = jwt.decode(token,key=app.config["SECRET_KEY"],algorithms=["HS256"])
		result=get_datas_from_db_3(data["user"])
		response=jsonify(result)
		return response
		
	return "ERRROR HERE"

@app.route("/api/getdatas/4",methods=["GET"])
@check_for_token
def getdatas4():
	if request.method == "GET":
		token = request.headers["authorization"]
		data = jwt.decode(token,key=app.config["SECRET_KEY"],algorithms=["HS256"])
		result=get_datas_from_db_4(data["user"])
		response=jsonify(result)
		return response
		
	return "ERRROR HERE"




@app.route("/api/getusersettings",methods=["GET"])
@check_for_token
def getusersettings():
	if request.method == "GET":
		token = request.headers["authorization"]
		
		try:
			data = jwt.decode(token,key=app.config["SECRET_KEY"],algorithms=["HS256"])
			result=get_account_settings(data["user"])
			print(result)
			response=jsonify({'username': result[0] , 'mail' : result[1]})
			return response
		except Exception as e:
			print(e)
			response=jsonify("error")
			return response,401
			
	return "ERRROR HERE"



@app.route("/api/changemail",methods=["POST"])
@check_for_token
def changeemailuser():
	if request.method == "POST":
		token = request.headers["authorization"]
		try:
			print(request.data)
			data = jwt.decode(token,key=app.config["SECRET_KEY"],algorithms=["HS256"])
			result=change_email_in_db(request.data.decode("utf-8"),data["user"])
			print(result)
			response=jsonify(result)
			return response
		except Exception as e:
			print(e)
			response=jsonify("error")
			return response,401
			
	return "ERRROR HERE"


@app.route("/api/changepassword",methods=["POST"])
@check_for_token
def changepassword():
	if request.method == "POST":
		token = request.headers["authorization"]
		try:
			print(request.json)
			data = jwt.decode(token,key=app.config["SECRET_KEY"],algorithms=["HS256"])
			result=change_password_in_db(request.json["actual_password"],request.json["new_password"],data["user"])
			print(result)
			response=jsonify(result)
			response.headers.add('Content-type', 'application/json')
			return response
		except Exception as e:
			print(e)
			response=jsonify("error")
			return response,401
			
	return "ERRROR HERE"	





if __name__=="__main__":
	print("CA TOURNE")
	app.run(host='0.0.0.0',debug=True)
	