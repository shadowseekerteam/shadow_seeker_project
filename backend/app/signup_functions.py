#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pymysql
from db_profile import DATABASE
from shadow_seeker_script import Shadow_Seeker
import random
import string
import threading

database = DATABASE()

def get_random_password_string(length):
    password_characters = string.ascii_letters + string.digits
    password = ''.join(random.choice(password_characters) for i in range(length))
    return password

def create_db(name):
	
	host = database.host
	user = database.user
	password = database.password
	db = database.database
	
	try:
		admin_con = pymysql.connect(host=host,user=user,password=password,db=db, use_unicode=True, charset='utf8')
		admin_cur = admin_con.cursor()
		new_database=name+'_db'
		new_user=name+'_user'
		new_passw=get_random_password_string(16)
		
		#Creation d'une BDD et d'un utilisateur. Tout est dédié pour chaque utilisateur (1 utilisateur = 1 BDD et un MySQL user) 
		admin_cur.execute("CREATE DATABASE IF NOT EXISTS {};".format(new_database))

		admin_cur.execute("CREATE USER IF NOT EXISTS '{}'@'%' IDENTIFIED WITH mysql_native_password BY '{}';".format(new_user,new_passw))
		admin_cur.execute("GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP ON {}.* TO '{}'@'%';".format(new_database,new_user))
		admin_cur.execute("FLUSH PRIVILEGES;")

		#Mise a jour des informations relatifs au clients pour les futurs connexions à sa BDD dediées
		requete_sql="UPDATE `informations_clients` SET `database_name` = '{}' , `database_user` = '{}' , `database_passw` = '{}'  WHERE client='{}';".format(new_database,new_user,new_passw,name)
		admin_cur.execute(requete_sql)
		admin_con.commit()

		admin_cur.execute("SELECT database_name,database_user,database_passw FROM informations_clients WHERE client='{}';".format(name))

		res = admin_cur.fetchone()
		user_database = res[0]
		userr = res[1]
		user_password=res[2]

		admin_con.close()

		try:
			con = pymysql.connect(host=host, user=userr, password=user_password, db=user_database, use_unicode=True, charset='utf8')
			cur = con.cursor()

			cur.execute("CREATE TABLE IF NOT EXISTS `informations` (date TIMESTAMP DEFAULT CURRENT_TIMESTAMP, domain varchar(100)) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
			cur.execute("CREATE TABLE IF NOT EXISTS `leaked_mail` (`atmail` varchar(350) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
			cur.execute("CREATE TABLE IF NOT EXISTS `atmail_on_pastebin` (`atmail` varchar(300) NOT NULL, `pastebinurl` varchar(200) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
			cur.execute("CREATE TABLE IF NOT EXISTS `hibp` (`atmail` varchar(300) NOT NULL, `date` varchar(10) NOT NULL , `source_of_leak` varchar(600) NOT NULL , `pwn_content` varchar(5000) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
			cur.execute("CREATE TABLE IF NOT EXISTS `holehe` (`atmail_hibp` varchar(300) NOT NULL, `content_holehe` varchar(3000) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
		except Exception as e:
			print("Error with user databse :"+ e,flush=True)

	except Exception as e:
		print(e,flush=True)
		return e


def signup_user_bdd(datas):
	
	host = database.host
	user = database.user
	password = database.password
	db = database.database
	
	
	try:
		con = pymysql.connect(host=host,user=user,password=password,db=db, use_unicode=True, charset='utf8')
		cur = con.cursor()
	except Exception as e:
		print(e,flush=True)
	
	statement=False
	if (datas["passphrase"] and datas["username"] and datas["email"] and datas["password"]):
		
		try :
			select_sql="SELECT * FROM users WHERE login=\"{}\";".format(datas["username"])
			print(select_sql)
			cur.execute(select_sql)
			
			if cur.fetchone():
				return "User already exist"
		except:
			return "Database error"
		
		
		try :
			select_sql="SELECT `passphrase`,`id` FROM verify_signup WHERE compagny_name=\"{}\";".format(datas["username"])
			print(select_sql)
			cur.execute(select_sql)
			
			res=cur.fetchone()
			right_pass = res[0]
			id_signup= res[1]
			
			while 1:
				if right_pass:
					if right_pass==datas["passphrase"]:
						statement=True
						break
					else:
						return "Wrong passphrase"
				else:
					return "Error with database"
		except Exception as e:
			print(e,flush=True)
			return "Company name not found"
		if (statement):
			result_main_script = "error"
			try:
				requete_sql="INSERT INTO `users` ( `id_signup`, `login`, `password`, `mail`) VALUES ({},\"{}\",SHA2(\"{}\",256),\"{}\");".format(id_signup,datas["username"],datas["password"],datas["email"])
				cur.execute(requete_sql)
				con.commit()
				
				#Appel de la fonctions pour creation du user MySQL et BDD dédiée
				requete_sql="INSERT INTO `informations_clients` ( `id_user`, `client`) VALUES ({},\"{}\");".format(id_signup,datas["username"])
				cur.execute(requete_sql)

				con.commit()
				con.close()

				print("OK ! Creation de database et user"+str(datas["username"]))
				create_db(datas["username"])


				#On lance le script principal dans un thread
				thread = threading.Thread(target=Shadow_Seeker.start, args=(datas["username"],))
				thread.start()
				result_main_script="success"
			except Exception as e:
				print(e,flush=True)
			return result_main_script
	else:
		con.close()
		return "at least one empty input"
		