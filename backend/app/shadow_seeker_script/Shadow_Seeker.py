import sqlite3
import os
import requests
import json
import holehe
import sys
import time
import pymysql
import subprocess

from db_profile import DATABASE

global database

database = DATABASE()



def get_user_db(client):
	host = database.host
	user = database.user
	password = database.password
	db = database.database
	try:
		admin_con = pymysql.connect(host=host, user=user, password=password, db=db, use_unicode=True, charset='utf8')
		admin_cur = admin_con.cursor()

		request_select="SELECT t1.database_user,t1.database_passw,t1.database_name,t2.domain_name FROM informations_clients AS t1 INNER JOIN verify_signup AS t2 ON t1.id_user = t2.id WHERE t1.client='{}'".format(client)
		admin_cur.execute(request_select)
		res = admin_cur.fetchone()

		userr = res[0]
		user_password = res[1]
		user_database = res[2]
		domain=res[3]

		admin_con.close()

		try:
			global con
			global cur
			con = pymysql.connect(host=host, user=userr, password=user_password, db=user_database, use_unicode=True,charset='utf8')
			cur = con.cursor()
		except Exception as e:
			print("1:"+str(e), flush=True)

	except Exception as e:
		print("2:"+str(e), flush=True)

	return domain




def Hibp_Search(mail):
	'''Cette fonction permet de recuperer toutes les informations relatives a une adresse mail via une requete sur le site have i been pwned'''
	HEADERS = {"User-Agent": "checkpwnedemails","hibp-api-key": "your_api_key_here",}#informations necessaire pour have i been pwned
	PWNED_API_URL = "https://haveibeenpwned.com/api/v3/{}/{}?truncateResponse={}".format("breachedaccount",mail,"yes")#formalisation de la requete
	r = requests.get(url=PWNED_API_URL, headers=HEADERS)
	if r.content:
		responses=r.json()
		print("Response HIPB : "+str(responses),flush=True)
		for response in responses:
			requete_sql="INSERT INTO `hibp` (`atmail`, `date`,`source_of_leak`,`pwn_content`) VALUES (\"{}\",\"{}\",\"{}\",\"{}\");".format(mail,response['BreachDate'],response['Name'],response['DataClasses'])
			cur.execute(requete_sql)
			con.commit()
				#print("Your mail was breached on "+response[i]['Name']+"")
			   #print("the date of the data breach is:"+response[i]['BreachDate']+"")
				#print("these informations may have been leaked:")
			#for j in range(len(response[i]['DataClasses'])):
				#print(""+response[i]['DataClasses'][j]+"")
			#print("\n")
	else:
		print("not pwned")

		
def mail_on_sites(mail):
	''' Cette fonction nous permet d'utiliser la librairie holehe afin de connaitre les sites sur le(s)quel(s) l'adresse email est utilisé pour un compte'''
	#Création de fichier.txt si il n'existe pas
	if not os.path.exists('fichier.txt'):
		with open('fichier.txt', 'w'): pass
	#Commande pour lancer holehe et mettre le résultat dans fichier.txt
	holehe_cmd = "holehe {} > fichier.txt".format(mail)
	os.system(holehe_cmd)
	#init list
	list=[]
	#ouverture du fichier.txt et récupération des sites OK
	file = open('fichier.txt', "r")
	for line in file:
		if line[5:7] == "[+":
			list.append(line[9:])
	file.close()

	#Suppresion de la merde à la fin de chaque nom de site
	tmp_list = []
	for Mastr in list:
		str_tmp = Mastr.replace("\x1b[0m\n", "")
		tmp_list.append(str_tmp)
	del tmp_list[-1]

	#liste finale | Peut-être faire un return etc selon implémentation
	#print(tmp_list)
	if not tmp_list:
		print("nous n'avons trouver aucun lien associé")
	else:
		requete_sql="INSERT INTO `holehe` (`atmail_hibp`,`content_holehe`) VALUES (\"{}\",\"{}\");".format(mail, str(tmp_list))
		print("holehe : "+str(mail)+" : "+ str(tmp_list), flush=True)
		cur.execute(requete_sql)
		con.commit()
		print("cet email est présent sur les sites suivants"+str(tmp_list)+"")

def Pastebin_search(search,object):#fonction de recherche via l'api de psdmp
	'''Cette fonction permet de recuperer les pastebins qui contiennent l'adresse mail, il ne seront pas forcement accessible'''
	try:
		url="https://psbdmp.ws/api/search/{}/{}".format(search,object)#formatage de la requete api
		response = requests.get(url)
		data=response.json()
		if data['count']==0: #si le champ count contient la valeur 0 alors aucun pastebin indexe n'existe sur l'element recherche
			print("nothing found for :"+object+"")
		else:
			print('Un leak potentiel existe pour:'+object,flush=True)
			print('Pastebin data : '+ data['data'], flush=True)
			for i in range(len(data['data'])):#affichage des url de pastebin en fonction de l'element recherche
				urler = "https://pastebin.com/"+data['data'][i]['id']+""#formalisation de l'url de pastebin avec les elements de psbdmp
				print('Ces informations ne sont pas forcement accessible ')
				requete_sql="INSERT INTO `atmail_on_pastebin` (`atmail`, `pastebinurl`) VALUES (\"{}\",\"{}\");".format(email,urler)
				print(requete_sql,flush=True)
				cur.execute(requete_sql)
				con.commit()
	except Exception as e:
		print("nous avons une erreur lors de la recherche")


def Extract_From_DB_And_Search(domain):
	'''Cette fonction permet de recuperer les adresses mails leakes dans la bdd de The Harvester''' 
	#extraction des @ mails présente dans la bdd de the harvester

	db=sqlite3.connect('/root/.local/share/theHarvester/stash.sqlite')#le nom de la base sqlite est stash.sqlite
	tab_mail=[]
	tab_mail_fin=[]
	connection = db.cursor()
	connection.execute("select resource,source from results where type='email'")
	recup=connection.fetchall()
	for i in range(len(recup)):
		tab_mail.append((recup[i][0],recup[i][1]))
	for mail in tab_mail :
		if mail not in tab_mail_fin:
			print("mail1 : "+str(mail),flush=True)
			tab_mail_fin.append(mail)

	#envoie a la fonction aui va generer le rapport. 
	#Le 1er parametre est la liste sous forme de dictionnaire [{key1 : value1 , key2 : value2},{key1 : value3 , key2 : value4},] qui sera integre qu rapport html.
	#le 2nd parametre represente le nom des clefs aui seront attribuees a chaues valeurs
	#La liste sera remplacee par une chaine de caractere dan le html generique. Le 3eme paramete est cette chaine a remplacer.
	db.close()

	Pastebin_search("domain", domain)
		

	#tab_mail_fin=[('direction@securiteoff.com', 'bing'), ('pixel-161294903278189-web-@securiteoff.com', 'exalead'), ('pixel-1612949031971171-web-@securiteoff.com', 'exalead'), ('conseiller@darty.com', 'google'), ('assurance.servicecommercial@darty.com', 'google'), ('noreply@sas-darty.com', 'google'), ('no-reply@commandes-darty.com', 'qwant'), ('conseiller@darty.com', 'qwant'), ('nepasrepondre@service-darty.com', 'qwant')]
	print("all email:" + str(tab_mail_fin), flush=True)
	for element in tab_mail_fin:
		at_mail = element[0]#tableau contenant toutes les adresses mails
		print("email:" + str(at_mail), flush=True)
		requete_sql="INSERT INTO `leaked_mail` (`atmail`) VALUES (\"{}\");".format(at_mail)
		cur.execute(requete_sql)
		con.commit()
		Pastebin_search("email", at_mail)
		Hibp_Search(at_mail)
		mail_on_sites(at_mail)
		print("-----------next mail--------------")
		time.sleep(1)


def Automat_The_Harvester(domain):
	'''fonction permettant d'automatiser la requete The Harvester'''

	tab=["baidu","bing","dnsdumpster","exalead","google","intelx","linkedin","linkedin_links","qwant","securitytrails","trello","yahoo"]#nous avons choisi de selectionner nos champs de recherches afin d'eviter les recherches abusives
	for i in range(len(tab)):
		cmd = "theHarvester -d "+domain+" -b "+str(tab[i])+""#cette ligne est a modifier si vous n\' avez pas the harvester dans le path
		os.system(cmd)
		time.sleep(2)


def start(client):
	'''fonction de depart du script'''
	time.sleep(1)
	try:
		domain=get_user_db(client)
	except:
		return "error"

	#lancement des deux grandes parties du script
	Automat_The_Harvester(domain)
	Extract_From_DB_And_Search(domain)


	con.close()

	return "success"