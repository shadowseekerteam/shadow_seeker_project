# Shadow_seeker
 
 
 
## Prérequis
Installation de the harvester:
https://github.com/laramies/theHarvester/wiki/Installation

API requises pour un fonctionnement minimal de the harvester dans notre cadre d’utilisation:
shodan
github 

Pour le script Shadow_Seeker, il faudra acheter l’accès à l’api have i been pwned, attention il faudra renouveler l’accès api tous les mois:
https://haveibeenpwned.com/API/Key

## Guide de déploiement

1- Déziper l’archive Shadow_Seeker.zip

2- Installer holehe via:

```pip3 install -r requirements.txt```

3- Installer theHarvester à partir de l’adresse suivante : https://github.com/laramies/theHarvester

4- Dans le fichier Shadow_Seeker.py, modifier la ligne suivante, présente dans la fonction Automat_The_Harvester, en remplaçant “your_path_to_the_harvester” par le chemin que vous souhaitez :

```cmd = "python3 /your_path_to_the_harvester/theHarvester/theHarvester.py -d "+domain+" -b all"```

Si The Harvester est installé dans vos sources:

```cmd = "theHarvester -d "+domain+" -b all"```

N’oubliez pas d’alimenter le fichier renseignant les API dont vous aurez besoin.

De même n’oubliez pas de renseigner la clé d’api have i been pwned dans Shadow_Seeker.py.
Voici la ligne contenant la clé d’api et une clé XXXX:
``` HEADERS = {"User-Agent": "checkpwnedemails","hibp-api-key": "XXXXXX",} ```

## Guide d'utilisation 

1- Lancez le fichier Shadow_Seeker.py avec python

python3 Shadow_seeker.py

2- Pour la recherche,  le nom de domaine sur lequel vous souhaitez faire des recherches de fuite sera fourni au programme via l’entrée standard.

Lorsque le programme le demande, entrez le nom de domaine sur lequel vous souhaitez faire des recherches de fuite de données.

/mettre un exemple en capture/

3- Un rapport sera créé en html au même niveau que le fichier Shadow_seeker.py
