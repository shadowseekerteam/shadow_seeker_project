SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `shadow_seeker` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `shadow_seeker`;

CREATE TABLE `verify_signup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `compagny_name` varchar(40) NOT NULL,
  `passphrase` varchar(100) NOT NULL,
  `domain_name` varchar(50) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `informations_clients` (
  `id_user` int(11) DEFAULT NULL,
  `client` varchar(30) NOT NULL,
  `database_name` varchar(30) DEFAULT NULL,
  `database_user` varchar(50) DEFAULT NULL,
  `database_passw` varchar(45) DEFAULT NULL,
  CONSTRAINT `informations_clients_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `verify_signup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `users` (
  `id_signup` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_signup`) REFERENCES `verify_signup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE USER IF NOT EXISTS 'shadow_seeker_user'@'%' IDENTIFIED WITH mysql_native_password  BY 'strongpassword';
GRANT ALL PRIVILEGES ON *.* TO 'shadow_seeker_user'@'%' WITH GRANT OPTION;

FLUSH PRIVILEGES;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
